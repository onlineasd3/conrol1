const t = require('node-fetch');
let text2 = "", text3 = "", c = 0;
const deleteScript = (s: string) => {
    var SCRIPT_REGEX = /()<[^<]*\)*<\/script>/gi;
    while (SCRIPT_REGEX.test(s)) {
        s = s.replace(SCRIPT_REGEX, "");
    }
    return s;
};
t('https://ru.wikipedia.org/wiki/TypeScript')
    .then(res => res.text())
    .then(body => {
        const dataWithoutScripts = deleteScript(body);
        const dataWithoutHtmlTags = dataWithoutScripts.replace(/<[^>]+>/g, '');
        const dataWithoutLinebreaks = dataWithoutHtmlTags.replace(/[\r\n]+/gm, '');
        const dataWithoutMultipleSpaces = dataWithoutLinebreaks.replace(/  +/g, ' ');
        const dataWithoutTabs = dataWithoutMultipleSpaces.replace(/\t+/g, ' ');
        const dataWithoutAnd = dataWithoutTabs.replace(/[0-9]\&\#[0-9][0-9]\;/g, '');
        const dataWithoutAnd1 = dataWithoutAnd.replace(/\&\#[0-9][0-9]\;/g, '');
        const dataWithoutAnd2 = dataWithoutAnd1.replace(/\&\#[0-9][0-9][0-9]\;/g, '');

        t.readFile('readed.txt','utf-8', function(err, dataWithoutAnd1){
            var arr;
                arr = dataWithoutAnd1.replace( /\D+/g, '');
        
            console.log("Количество цифр = ", arr.length); // Вывод
        });
        
    });

var t = require('node-fetch');
var text2 = "", text3 = "", c = 0;
var deleteScript = function (s) {
    var SCRIPT_REGEX = /()<[^<]*\)*<\/script>/gi;
    while (SCRIPT_REGEX.test(s)) {
        s = s.replace(SCRIPT_REGEX, "");
    }
    return s;
};
t('https://ru.wikipedia.org/wiki/TypeScript')
    .then(function (res) { return res.text(); })
    .then(function (body) {
    var dataWithoutScripts = deleteScript(body);
    var dataWithoutHtmlTags = dataWithoutScripts.replace(/<[^>]+>/g, '');
    var dataWithoutLinebreaks = dataWithoutHtmlTags.replace(/[\r\n]+/gm, '');
    var dataWithoutMultipleSpaces = dataWithoutLinebreaks.replace(/  +/g, ' ');
    var dataWithoutTabs = dataWithoutMultipleSpaces.replace(/\t+/g, ' ');
    var dataWithoutAnd = dataWithoutTabs.replace(/[0-9]\&\#[0-9][0-9]\;/g, '');
    var dataWithoutAnd1 = dataWithoutAnd.replace(/\&\#[0-9][0-9]\;/g, '');
    var dataWithoutAnd2 = dataWithoutAnd1.replace(/\&\#[0-9][0-9][0-9]\;/g, '');
    t.readFile('readed.txt', 'utf-8', function (err, dataWithoutAnd1) {
        var arr;
        arr = dataWithoutAnd1.replace(/\D+/g, '');
        console.log("Количество цифр = ", arr.length); // Вывод
    });
});
